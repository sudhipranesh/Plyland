package in.papercrane.plyland.data.menus.customerList.customerPayment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Payments;

/**
 * Created by PAPERCRANE on 25-01-2018.
 */

public class CustomerPaymentAdapter extends RecyclerView.Adapter<CustomerPaymentAdapter.CustomerViewHolder> {

    private ArrayList<Payments> mPaymentList;
    Context mContext;

    public CustomerPaymentAdapter(ArrayList<Payments> payments, Context context) {

        this.mPaymentList = payments;
        this.mContext = context;
    }


    @Override
    public CustomerPaymentAdapter.CustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.activity_cust_paymentlist_item, parent, false);

        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomerPaymentAdapter.CustomerViewHolder holder, int position) {
        Payments payments = mPaymentList.get(position);


    }

    @Override
    public int getItemCount() {
        return mPaymentList.size();
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_cust_pay_date)
        TextView txtDate;
        @BindView(R.id.txt_cust_pay_amnt)
        TextView txtAmount;
        @BindView(R.id.txt_cust_pay_desc)
        TextView txtDescription;

        public CustomerViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
