//package in.papercrane.plyland.data.menus.login;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
////import android.provider.ContactsContract;
//import android.text.method.PasswordTransformationMethod;
//import android.text.method.PasswordTransformationMethod;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.bumptech.glide.Glide;
//
////import java.util.HashMap;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//import in.papercrane.plyland.R;
//import in.papercrane.plyland.data.SessionManager;
//import in.papercrane.plyland.data.Utility;
//import in.papercrane.plyland.data.base.BaseActivity;
//
////import in.papercrane.plyland.data.menus.register.RegisterActivity;
////import in.papercrane.plyland.data.menus.register.RegisterActivity;
//import in.papercrane.plyland.data.menus.splashScreen.SplashScreenActivity;
//import in.papercrane.plyland.data.models.User;
//
///**
// * Created by PAPERCRANE on 19-02-2018.
// */
//
//public class LoginActivity extends BaseActivity {
//
//    private static final String USER = "user";
//    private static final String USER_LOGIN = "userLogin";
//    private static final String EMAIL = "email";
//    private static final String PASSWORD = "password";
//
//    @BindView(R.id.ed_username)
//    EditText mUsername;
//    @BindView(R.id.ed_reg_password)
//    EditText mPassword;
//    @BindView(R.id.btn_user_login)
//    Button btnLogin;
//    @BindView(R.id.img_login)
//    ImageView imLogin;
//    @BindView(R.id.img_password_toggle)
//    ImageView mTogglePasswordVisible;
//    @BindView(R.id.toggle_password_hidden)
//    ImageView mTogglePasswordHidden;
////    @BindView(R.id.btn_reg_activity)
////    Button btnRegister;
//
//    private String userName;
//    private String password;
//    User mUser;
//
//    SessionManager mSessionManager;
//
//    public static void start(Context context) {
//        Intent starter = new Intent(context, LoginActivity.class);
//        starter.putExtra(USER_LOGIN, true);
//        context.startActivity(starter);
//    }
//
//    @Override
//    protected void init() {
//
////        mUser = getIntent().getParcelableExtra(USER);
//
//        Glide.with(this)
//                .load(R.drawable.bg_login)
//                .into(imLogin);
//
//        mSessionManager = new SessionManager(getApplicationContext());
//
//        Toast.makeText(this, "User Login"
//                + mSessionManager.isloggedIn(), Toast.LENGTH_SHORT).show();
//
//        mTogglePasswordHidden.setVisibility(View.GONE);
//
//    }
//
////    @OnClick(R.id.btn_reg_activity)
////    public void startRegisterActivity() {
////
////        RegisterActivity.start(this);
////    }
//
//
//    @OnClick(R.id.btn_user_login)
//    public void login() {
//
////        if (validateEmail(mUsername.toString())) {
//
//        setUpLogin();
////        }
//
//
//    }
//
////    public boolean validateEmail(String email) {
////
////        if (Utility.isValidEmail(email)) {
////
////            return false;
////        } else {
////
////            Toast.makeText(this, "Enter a Valid Email", Toast.LENGTH_SHORT).show();
////        }
////
////        return true;
////    }
//
//    private void setUpLogin() {
//
//        userName = mUsername.getText().toString();
//        password = mPassword.getText().toString();
//
//        if (userName.isEmpty() && password.isEmpty()) {
//            Toast.makeText(this, "Username or Password Field is Empty", Toast.LENGTH_SHORT).show();
//
//        } else {
//
//            userLogin();
//        }
//    }
//
//    private void userLogin() {
//
//        SessionManager ss = new SessionManager(getApplicationContext());
//
//        if ((ss.getEmail() != null) && ss.getPassword() != null) {
//
//            if (userName.equals(ss.getEmail()) && password.equals(ss.getPassword())) {
//
//
//                SplashScreenActivity.start(this);
//                finish();
//
//            } else {
//
//                Toast.makeText(this, "Login Error", Toast.LENGTH_SHORT).show();
//            }
//        } else {
//
//            ss.setCredentials(userName, password);
//            Toast.makeText(this, "User Logged In", Toast.LENGTH_SHORT).show();
//            SplashScreenActivity.start(this);
//        }
//
//
////        if (userName.trim().length() > 0 && password.trim().length() > 0) {
////
////            if ((userName == mSessionManager.getEmail()) && (password == mSessionManager.getPassword())) {
////
////                SessionManager ss = new SessionManager(getApplicationContext());
////
////                ss.getLoginUser(userName, password);
////                SplashScreenActivity.start(this);
////
////
//////                mSessionManager.getEmail();
//////                mSessionManager.getPassword();
////            } else {
////
////                Toast.makeText(this, "Login Error, Use valid Email or Password", Toast.LENGTH_SHORT).show();
////            }
////        } else {
////
////            Toast.makeText(this, "Invalid Email or Password", Toast.LENGTH_SHORT).show();
////        }
//    }
//
//    @Override
//    public void onBackPressed() {
//
//        finish();
//    }
//
//    @OnClick(R.id.img_password_toggle)
//    public void togglePasswordVisible() {
//        mPassword.setTransformationMethod(null);
//        mTogglePasswordVisible.setVisibility(View.GONE);
//        mTogglePasswordHidden.setVisibility(View.VISIBLE);
//    }
//
//    @OnClick(R.id.toggle_password_hidden)
//    public void togglePasswordHide() {
//
//        mPassword.setTransformationMethod(new PasswordTransformationMethod());
//        mTogglePasswordHidden.setVisibility(View.GONE);
//        mTogglePasswordVisible.setVisibility(View.VISIBLE);
//
//    }
//
//    @Override
//    public int setLayout() {
//        return R.layout.activity_login;
//    }
//}
