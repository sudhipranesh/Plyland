package in.papercrane.plyland.data.menus.customerList.customerPayment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;

import butterknife.BindView;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseFragment;
import in.papercrane.plyland.data.models.Payments;

/**
 * Created by PAPERCRANE on 25-01-2018.
 */

public class CustomerPaymentFragment extends BaseFragment {

    private static final String PAYMENT_LIST = "paymentList";
    private static final String PAYMENTS = "payments";

    @BindView(R.id.toolbar_cust_pay)
    Toolbar mToolbar;
    @BindView(R.id.rv_cust_payment_list)
    RecyclerView mCustPaymentList;

    CustomerPaymentAdapter mAdapter;
    ArrayList<Payments> mPayment = new ArrayList<>();
    Payments payments;
    Context mContext;


    public static CustomerPaymentFragment newInstance() {
        CustomerPaymentFragment fragment = new CustomerPaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<Payments> getPayments() {
        ArrayList<Payments> payments = new ArrayList<>();
        payments.addAll(SQLite.select().from(Payments.class).queryList());
        return payments;
    }

    @Override
    protected void init() {

        payments = getArguments().getParcelable(PAYMENTS);

        mAdapter = new CustomerPaymentAdapter(mPayment, mContext);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mCustPaymentList.setLayoutManager(mLayoutManager);
        mCustPaymentList.setItemAnimator(new DefaultItemAnimator());
        mCustPaymentList.setAdapter(mAdapter);

    }

    @Override
    protected int setLayout() {
        return R.layout.activity_customer_payment;
    }
}
