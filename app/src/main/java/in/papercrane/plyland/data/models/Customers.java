package in.papercrane.plyland.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.ViewDebug;

import in.papercrane.plyland.data.database.PlylandDB;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PAPERCRANE on 17-01-2018.
 */
@Table(database = PlylandDB.class)
public class Customers extends BaseModel implements Parcelable {


    @PrimaryKey(autoincrement = true)
    public int id;
    @Column
    public String name;
    @Column
    public String email;
    @Column
    public String phone;
    @Column
    public double openingbalance;
    @Column
    public String details;


    public List<Payments> payments;

    public List<Credits>credits;

    public List<Payments> getPayments() {// TODO: 24/4/17
        if (payments == null || payments.isEmpty())
            payments = SQLite.select()
                    .from(Payments.class)
                    .where(Payments_Table.customers_id.eq(this.id))
                    .queryList();
        return payments;
    }

    public List<Credits> getCredits() {// TODO: 24/4/17
        if (credits == null || credits.isEmpty())
            credits = SQLite.select()
                    .from(Credits.class)
                    .where(Credits_Table.customers_id.eq(this.id))
                    .queryList();
        return credits;
    }

    public Customers() {
    }

    @Override
    public boolean save() {
        return super.save();
    }

    @Override
    public String toString() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeDouble(this.openingbalance);
        dest.writeString(this.details);
        dest.writeTypedList(this.payments);
    }

    protected Customers(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.openingbalance = in.readDouble();
        this.details = in.readString();

    }

    public static final Creator<Customers> CREATOR = new Creator<Customers>() {
        @Override
        public Customers createFromParcel(Parcel source) {
            return new Customers(source);
        }

        @Override
        public Customers[] newArray(int size) {
            return new Customers[size];
        }
    };
}
