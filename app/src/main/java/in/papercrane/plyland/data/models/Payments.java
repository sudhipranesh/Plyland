package in.papercrane.plyland.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import in.papercrane.plyland.data.database.PlylandDB;
import com.raizlabs.android.dbflow.annotation.Column;

import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;



/**
 * Created by PAPERCRANE on 17-01-2018.
 */
@Table(database = PlylandDB.class)
public class Payments extends BaseModel  implements Parcelable{

    @PrimaryKey(autoincrement = true)
    public int id;
    @Column
    public double amount;
    @Column
    public String date;
    @Column
    public String description;
    @Column
    @ForeignKey(saveForeignKeyModel = true)
    public Customers customers;


    public Payments() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.amount);
        dest.writeString(this.date);
        dest.writeString(this.description);
        dest.writeParcelable(this.customers, flags);
    }

    protected Payments(Parcel in) {
        this.id = in.readInt();
        this.amount = in.readDouble();
        this.date = in.readString();
        this.description = in.readString();
        this.customers = in.readParcelable(Customers.class.getClassLoader());
    }

    public static final Creator<Payments> CREATOR = new Creator<Payments>() {
        @Override
        public Payments createFromParcel(Parcel source) {
            return new Payments(source);
        }

        @Override
        public Payments[] newArray(int size) {
            return new Payments[size];
        }
    };
}
