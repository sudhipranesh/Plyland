package in.papercrane.plyland.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Papercrane on 30-01-2018.
 */

public class Utility {


    public static boolean isValidEmail(CharSequence target) {
        return  android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public  static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
