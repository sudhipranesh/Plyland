package in.papercrane.plyland.data.menus.paymentList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Payments;

/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentsViewHolder> {


    Context mContext;
    ArrayList<Payments> mPaymentList = new ArrayList<>();


    public PaymentListAdapter(ArrayList<Payments> payments, Context context) {

        mContext = context;
        mPaymentList = payments;
    }

    @Override
    public PaymentListAdapter.PaymentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.activity_payment_list_item, parent, false);

        return new PaymentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentListAdapter.PaymentsViewHolder holder, int position) {

        Payments payments = mPaymentList.get(position);
        holder.txtCustomer.setText(payments.customers.name);
        holder.txtDate.setText(payments.date);
        holder.txtAmount.setText(String.valueOf(payments.amount));
        holder.txtDescription.setText(payments.description);

    }



    @Override
    public int getItemCount() {
        return mPaymentList.size();
    }

    public class PaymentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_cust_Name)
        TextView txtCustomer;
        @BindView(R.id.txt_date)
        TextView txtDate;
        @BindView(R.id.txt_payment_amnt)
        TextView txtAmount;
        @BindView(R.id.txt_pay_desc)
        TextView txtDescription;

        public PaymentsViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
