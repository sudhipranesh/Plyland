package in.papercrane.plyland.data.menus.reports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Report;

/**
 * Created by PAPERCRANE on 19-01-2018.
 */

public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.ReportsViewHolder> {

    public Context mContext;

    private List<Report> mReport;

    public ReportsAdapter(Context context, ArrayList<Report> reportArrayList) {

        mContext = context;
        this.mReport = reportArrayList;

    }

    @Override
    public ReportsAdapter.ReportsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.activity_reposrt_item, parent, false);

        return new ReportsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ReportsAdapter.ReportsViewHolder holder, int position) {
        Report report = mReport.get(position);
        holder.mPhoneNumber.setText(report.phone);
        holder.mCredits.setText(String.valueOf(report.totalCredits));
        holder.mPayments.setText(String.valueOf(report.totalPayments));
        holder.mOutstandingBal.setText(String.valueOf(report.outstandingBalance));
        holder.mCustomerName.setText(report.customers.name);

    }


    @Override
    public int getItemCount() {
        return mReport.size();
    }

    public class ReportsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_credit_cust_name)
        TextView mCustomerName;
        @BindView(R.id.txt_phone_number)
        TextView mPhoneNumber;
        @BindView(R.id.txt_credits)
        TextView mCredits;
        @BindView(R.id.txt_payments)
        TextView mPayments;
        @BindView(R.id.txt_outstandingBal)
        TextView mOutstandingBal;

        public ReportsViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
