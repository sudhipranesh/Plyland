package in.papercrane.plyland.data.menus.customerList.modification;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseBottomSheet;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;

/**
 * Created by PAPERCRANE on 25-01-2018.
 */

public class AddCustomerPaymentFragment extends BaseBottomSheet {

    @BindView(R.id.txt_Select_pay_Date)
    TextView txtSelectDate;
    @BindView(R.id.ed_cust_pay_amnt)
    EditText edAmount;
    @BindView(R.id.ed_cust_pay_desc)
    EditText edDescription;

    public static final String CUSTOMERID= "customerId";
    private String mSelectedDate = null;
    private Customers customerId;

    public static AddCustomerPaymentFragment newInsatnce(Customers customerId) {
        AddCustomerPaymentFragment fragment = new AddCustomerPaymentFragment();
        Bundle args = new Bundle();
        args.putParcelable(CUSTOMERID, customerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void init() {

      customerId =   getArguments().getParcelable(CUSTOMERID);

    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_add_customer_payment;
    }

    @OnClick(R.id.im_close_cust_pay)
    public void close() {
        dismiss();
    }

    @OnClick(R.id.txt_Select_pay_Date)
    public void openDatePicker() {

        showDatePicker(txtSelectDate);

    }

    private void showDatePicker(final TextView txtSelectDate) {

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (datePicker, year, month, dayOfMonth) -> {

                    txtSelectDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    mSelectedDate = formatDate(year, (month + 1), dayOfMonth);

                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private String formatDate(int year, int month, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        c.set(year, month, dayOfMonth);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(date);
    }

    @OnClick(R.id.btn_submit_cust_pay)
    public void addPaymentForCustomer(){
        Payments payments = new Payments();
        payments.date = mSelectedDate;
        payments.amount = Double.parseDouble(edAmount.getText().toString());
        payments.description = edDescription.getText().toString();
        payments.customers = customerId;
        payments.save();
        dismiss();

        Toast.makeText(getContext(), "Payment Added", Toast.LENGTH_LONG).show();
    }


}
