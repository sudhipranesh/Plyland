package in.papercrane.plyland.data.menus.splashScreen;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.menus.home.HomeActivity;


/**
 * Created by PAPERCRANE on 02-02-2018.
 */

public class SplashScreenActivity extends BaseActivity {

    @BindView(R.id.iv_splash_test)
    ImageView imgSplash;

    public static void start(Context context) {

        Intent starter = new Intent(context, SplashScreenActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void init() {

        Glide.with(this)
                .load(R.drawable.splash_test)
                .into(imgSplash);

        switchToHome();
    }

    private void switchToHome() {

        HomeActivity.start(this);
    }

    @Override
    public int setLayout() {
        return R.layout.activity_splash;
    }


}
