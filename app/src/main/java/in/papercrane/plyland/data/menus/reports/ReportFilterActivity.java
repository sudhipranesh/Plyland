package in.papercrane.plyland.data.menus.reports;

import android.content.Context;
import android.content.Intent;
import android.support.v4.media.RatingCompat;
import android.support.v7.widget.RecyclerView;
import android.widget.Toolbar;

import java.util.ArrayList;

import butterknife.BindView;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.menus.home.ReportListActivity;
import in.papercrane.plyland.data.models.Report;

/**
 * Created by PAPERCRANE on 14-02-2018.
 */

public class ReportFilterActivity extends BaseActivity {

    private static final String REPORT_FILTER = "reportFilter";

    @BindView(R.id.filter_toolbar)
    android.support.v7.widget.Toolbar mToolbar;
    @BindView(R.id.rv_filter_reportlist)
    RecyclerView mFilterReportList;

    private ArrayList<Report> mReports = new ArrayList<>();
    private ReportFilterAdapter mAdapter;

    Report mReport;

    public static void start(Context context, Report report) {
        Intent starter = new Intent(context, ReportFilterActivity.class);
        starter.putExtra(REPORT_FILTER, report);
        context.startActivity(starter);
    }

    @Override
    protected void init() {

        mReport = getIntent().getParcelableExtra(REPORT_FILTER);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Report Filter");
            mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
            mToolbar.setNavigationOnClickListener(view -> finish());
        }

    }

    @Override
    public int setLayout() {
        return R.layout.activity_report_filter;
    }
}
