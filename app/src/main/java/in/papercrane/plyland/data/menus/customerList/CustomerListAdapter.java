package in.papercrane.plyland.data.menus.customerList;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;


import com.raizlabs.android.dbflow.config.FlowManager;

import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.App;
import in.papercrane.plyland.data.menus.creditList.CreditListActivity;
import in.papercrane.plyland.data.menus.customerList.modification.AddCustomerCreditFragment;
import in.papercrane.plyland.data.menus.customerList.modification.AddCustomerFragment;
import in.papercrane.plyland.data.menus.customerList.modification.AddCustomerPaymentFragment;
import in.papercrane.plyland.data.menus.home.ReportListActivity;
import in.papercrane.plyland.data.menus.paymentList.PaymentListActivity;
import in.papercrane.plyland.data.models.Customers;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerViewHolder> {

    private static final String ADD_CUST_PAY = "addCustomerPayment";
    private static final String ADD_CUST_CREDIT = "addCustomerCredit";


    private ArrayList<Customers> mCustomerList;
    Context mContext;

    public CustomerListAdapter(ArrayList<Customers> customers, Context context) {
        mCustomerList = customers;
        mContext = context;
    }

    @Override
    public CustomerListAdapter.CustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customerlist_item, parent, false);

        return new CustomerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomerListAdapter.CustomerViewHolder holder, int position) {
        Customers customers = mCustomerList.get(position);
        holder.txtName.setText(customers.name);
        holder.txtEmail.setText(customers.email);
        holder.txtPhone.setText(customers.phone);
        holder.txtOpenBalance.setText(String.valueOf(customers.openingbalance));
        holder.btnMore.setOnClickListener(view -> {

            PopupMenu popupMenu = new PopupMenu(mContext, holder.btnMore);
            popupMenu.getMenuInflater().inflate(R.menu.more_option_popup, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {

                if (item.getItemId() == R.id.add_payment) {

                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment frag = ((AppCompatActivity) mContext).getSupportFragmentManager().findFragmentByTag(ADD_CUST_PAY);
                    if (frag != null) {

                        ft.remove(frag);
                    }
                    ft.addToBackStack(null);

                    DialogFragment fragment = AddCustomerPaymentFragment.newInsatnce(customers);
                    fragment.show(ft, ADD_CUST_PAY);

                } else if (item.getItemId() == R.id.add_credit) {

                    FragmentTransaction ft = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                    Fragment frag = ((AppCompatActivity) mContext).getSupportFragmentManager().findFragmentByTag(ADD_CUST_CREDIT);
                    if (frag != null) {
                        ft.remove(frag);
                    }
                    ft.addToBackStack(null);

                    DialogFragment newFragment = AddCustomerCreditFragment.newInstance(customers);
                    newFragment.show(ft, ADD_CUST_CREDIT);


                } else if (item.getItemId() == R.id.view_payments) {
                    PaymentListActivity.start(mContext, customers);


                }

                else if (item.getItemId() == R.id.view_reports){
                    ReportListActivity.start(mContext, customers);
                }


                else  {
                    CreditListActivity.start(mContext,customers);
                }

                return true;
            });

            popupMenu.show();
        });
    }

    public void addCustomer(List<Customers> customers) {

        this.mCustomerList.addAll(customers);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mCustomerList.size();
    }


    public class CustomerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_cust_name)
        TextView txtName;
        @BindView(R.id.txt_cust_mail)
        TextView txtEmail;
        @BindView(R.id.txt_cust_phone)
        TextView txtPhone;
        @BindView(R.id.txt_op_balance)
        TextView txtOpenBalance;
        @BindView(R.id.btn_more)
        Button btnMore;


        public CustomerViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
