package in.papercrane.plyland.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import in.papercrane.plyland.data.database.PlylandDB;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by PAPERCRANE on 17-01-2018.
 */
@Table(database = PlylandDB.class)
public class Credits extends BaseModel implements Parcelable {

    @PrimaryKey(autoincrement = true)
    public int id;
    @ForeignKey(saveForeignKeyModel = true)
    public Customers customers;
    @Column
    public String description;
    @Column
    public Double amount;
    @Column
    public String date;

    public Credits() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeParcelable(this.customers, flags);
        dest.writeString(this.description);
        dest.writeDouble(this.amount);
        dest.writeString(this.date);
    }

    protected Credits(Parcel in) {
        this.id = in.readInt();
        this.customers = in.readParcelable(Customers.class.getClassLoader());
        this.description = in.readString();
        this.amount = in.readDouble();
        this.date = in.readString();
    }

    public static final Parcelable.Creator<Credits> CREATOR = new Parcelable.Creator<Credits>() {
        @Override
        public Credits createFromParcel(Parcel source) {
            return new Credits(source);
        }

        @Override
        public Credits[] newArray(int size) {
            return new Credits[size];
        }
    };
}
