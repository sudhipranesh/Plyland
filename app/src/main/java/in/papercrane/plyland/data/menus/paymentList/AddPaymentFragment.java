package in.papercrane.plyland.data.menus.paymentList;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.inspector.helper.IntegerFormatter;
import com.jakewharton.rxbinding2.InitialValueObservable;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import in.papercrane.plyland.R;

import in.papercrane.plyland.data.base.BaseBottomSheet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by PAPERCRANE on 18-01-2018.
 */

public class AddPaymentFragment extends BaseBottomSheet {

    @BindView(R.id.sp_customer)
    Spinner spCustomer;
    @BindView(R.id.ed_amount)
    EditText edAmount;
    @BindView(R.id.ed_desc)
    EditText edDescription;
    @BindView(R.id.txt_select_date)
    TextView txtSelectDate;

    private String mSelectedDate = null;

    private Disposable mDisposable;

    ArrayAdapter<Customers> cityArrayAdapter;


    Customers mSelectedCustomer = null;

    public static AddPaymentFragment newInstatnce() {
        AddPaymentFragment fragment = new AddPaymentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<Customers> getCustomers() {
        ArrayList<Customers> customers = new ArrayList<>();
        List<Customers> customerList = SQLite.select()
                .from(Customers.class)
                .queryList();
        customers.addAll(customerList);
        return customers;
    }

    @Override
    protected void init() {

        ArrayAdapter<Customers> customerAdapter = new ArrayAdapter<>
                (getContext(), android.R.layout.simple_spinner_item, getCustomers());
        customerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCustomer.setAdapter(customerAdapter);

    }


    @Override
    protected int setLayout() {
        return R.layout.fragment_add_payments;
    }


    @OnClick(R.id.iv_close_payments)
    public void close() {
        dismiss();
    }

    @OnClick(R.id.txt_select_date)
    public void selectDate() {
        openDatePicker(txtSelectDate);
    }

    private void openDatePicker(final TextView tvSelectDate) {

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (datePicker, year, month, dayOfMonth) -> {

                    tvSelectDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    mSelectedDate = formatDate(year, (month + 1), dayOfMonth);
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private String formatDate(int year, int month, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        c.set(year, month, dayOfMonth);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    @OnClick(R.id.btn_submit_payments)
    public void addPayments() {

        String amount = edAmount.getText().toString();
        String date = mSelectedDate;
        String description = edDescription.getText().toString();
        if (spCustomer == null)
            Toast.makeText(getActivity(), "Add a customer", Toast.LENGTH_SHORT).show();
        if (validateFields(amount, description, date)) {


            Payments payments = new Payments();
            payments.amount = Double.parseDouble(edAmount.getText().toString());
            payments.date = mSelectedDate;
            payments.description = edDescription.getText().toString();
            payments.customers = (Customers) spCustomer.getSelectedItem();
            payments.save();

            dismiss();
            Toast.makeText(getActivity(), "Payment Added", Toast.LENGTH_LONG).show();

        }
    }

    public boolean validateFields(String amount, String description, String date) {
        if (amount.length() > 0 && description.length() > 0 &&
                date.length() > 0) {
            return true;
        } else {
            Toast.makeText(getActivity(), "Enter all fields", Toast.LENGTH_SHORT).show();
            return false;
        }
    }


}
