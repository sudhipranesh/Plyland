package in.papercrane.plyland.data.menus.customerList.modification;


import android.database.Observable;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.raizlabs.android.dbflow.config.FlowManager;

import in.papercrane.plyland.R;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.Utility;
import in.papercrane.plyland.data.base.BaseBottomSheet;


import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.data.models.Customers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * Created by PAPERCRANE on 17-01-2018.
 */

public class AddCustomerFragment extends BaseBottomSheet {

    @BindView(R.id.ed_cust_name)
    EditText edName;
    @BindView(R.id.ed_cust_email)
    EditText edEmail;
    @BindView(R.id.ed_cust_phone)
    EditText edPhone;
    @BindView(R.id.ed_description)
    EditText edDetails;
    @BindView(R.id.ed_open_balance)
    EditText edOpenBalance;
    @BindView(R.id.btn_submit_cust)
    Button mCreateCustomer;

    private Disposable mDisposable;

    public static AddCustomerFragment newInstance() {
        AddCustomerFragment fragment = new AddCustomerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {

        io.reactivex.Observable<String> nameObservable = RxTextView.textChanges(edName)
                .map(CharSequence::toString)
                .doOnNext(s -> validate(s, edName));

        io.reactivex.Observable<String> phoneNuberObservable = RxTextView.textChanges(edPhone)
                .map(CharSequence::toString)
                .doOnNext(s -> validate(s, edPhone));

        mDisposable = io.reactivex.Observable.combineLatest(nameObservable, phoneNuberObservable,
                (s, s2) -> s.isEmpty() || s2.isEmpty())
                .subscribe(aBoolean -> mCreateCustomer.setEnabled(!aBoolean),
                        Timber::e);

    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_add_customer;
    }

    @OnClick(R.id.iv_close_add_cust)
    public void close() {

        dismiss();
    }

    @OnClick(R.id.btn_submit_cust)
    public void addCustomer() {
        if (validateFields(edEmail.getText().toString(), edPhone.getText().toString())) {

            Customers customers = new Customers();
            customers.name = edName.getText().toString();
            customers.email = edEmail.getText().toString();
            customers.openingbalance = Double.parseDouble(edOpenBalance.getText().toString());
            customers.details = edDetails.getText().toString();
            customers.phone = edPhone.getText().toString();
            customers.save();
            dismiss();

        }
    }

    private void validate(String s, TextView textView) {
        if (TextUtils.isEmpty(s)) {
            textView.setError("cannot be empty");
        } else textView.setError(null);
    }


    public boolean validateFields(String email, String mobNum) {
        if (email.length() > 0 && mobNum.length() > 0) {

            if (Utility.isValidEmail(email)) {

                if (Utility.isValidMobile(mobNum)) {
                    return true;
                } else {
                    Toast.makeText(getActivity(), "Enter a valid mobile number", Toast.LENGTH_SHORT).show();
                    return false;
                }

            } else {
                Toast.makeText(getActivity(), "Enter a valid email", Toast.LENGTH_SHORT).show();
                return false;
            }


        } else

            return false;

    }

}

