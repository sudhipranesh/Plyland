package in.papercrane.plyland.data.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by PAPERCRANE on 18-01-2018.
 */

@Database(name = PlylandDB.NAME, version = PlylandDB.VERSION)
public class PlylandDB {
    public static final String NAME = "plyland";
    public static final int VERSION = 1;
}
