package in.papercrane.plyland.data.menus.creditList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Credits;

/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class CreditListAdapter extends RecyclerView.Adapter<CreditListAdapter.CreditsViewHolder> {

    private ArrayList<Credits> mCreditList;
    Context mContext;

    public CreditListAdapter(ArrayList<Credits> credits, Context context) {

        mCreditList = credits;
        mContext = context;
    }

    @Override
    public CreditListAdapter.CreditsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.activity_credit_list_item, parent, false);

        return new CreditsViewHolder(view);
    }

    public void addCredits(List<Credits> credits) {
        this.mCreditList.addAll(credits);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(CreditListAdapter.CreditsViewHolder holder, int position) {
        Credits credits = mCreditList.get(position);
        String name = credits.customers.name;
        Log.d("name", name);
        holder.txtCustomer.setText(credits.customers.name);
        holder.txtAmount.setText(String.valueOf(credits.amount));
        holder.txtDate.setText(credits.date);
        holder.txtDescription.setText(credits.description);

    }

    @Override
    public int getItemCount() {
        return mCreditList.size();
    }

    public class CreditsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_credit_cust_name)
        TextView txtCustomer;
        @BindView(R.id.txt_credit_amount)
        TextView txtAmount;
        @BindView(R.id.txt_credit_date)
        TextView txtDate;
        @BindView(R.id.txt_credit_desc)
        TextView txtDescription;

        public CreditsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
