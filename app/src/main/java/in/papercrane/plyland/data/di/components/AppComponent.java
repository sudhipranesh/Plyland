package in.papercrane.plyland.data.di.components;

import in.papercrane.plyland.data.App;

/**
 * Created by PAPERCRANE on 22-01-2018.
 */


public interface AppComponent {

    void inject(App app);
}
