package in.papercrane.plyland.data.menus.reports;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Report;

/**
 * Created by PAPERCRANE on 14-02-2018.
 */

public class ReportFilterAdapter extends RecyclerView.Adapter<ReportFilterAdapter.ReportViewHolder> {

    Context mContext;
    ArrayList<Report> mReportList;

    public ReportFilterAdapter(Context context, ArrayList<Report> reports) {

        this.mContext = context;
        this.mReportList = reports;
    }

    @Override
    public ReportFilterAdapter.ReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.activity_report_filter_item, parent, false);

        return new ReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReportFilterAdapter.ReportViewHolder holder, int position) {
        Report report = mReportList.get(position);
        holder.txtCust.setText(report.customers.name);
        holder.txtPhone.setText(report.phone);
        holder.txtTotalPayments.setText(String.valueOf(report.totalPayments));
        holder.txtTotalCredit.setText(String.valueOf(report.totalCredits));
        holder.txtOutStandingBalance.setText(String.valueOf(report.outstandingBalance));
    }

    @Override
    public int getItemCount() {
        return mReportList.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_filter_cust)
        TextView txtCust;
        @BindView(R.id.txt_filter_phone)
        TextView txtPhone;
        @BindView(R.id.txt_filter_payments)
        TextView txtTotalPayments;
        @BindView(R.id.txt_filter_total_credit)
        TextView txtTotalCredit;
        @BindView(R.id.txt_filter_out_balance)
        TextView txtOutStandingBalance;

        public ReportViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(itemView);
        }
    }
}
