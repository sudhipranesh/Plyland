package in.papercrane.plyland.data;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;

import in.papercrane.plyland.BuildConfig;
import in.papercrane.plyland.data.di.components.AppComponent;
import timber.log.Timber;


public class App extends Application {

    public static AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fresco.initialize(getApplicationContext());
        FlowManager.init(new FlowConfig.Builder(this).build());
        debugSetup();

    }

    private void debugSetup() {
        if (BuildConfig.DEBUG) {
            FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
            Stetho.initializeWithDefaults(this);
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
