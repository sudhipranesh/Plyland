//package in.papercrane.plyland.data.menus.creditList.customerCredit;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//
//import com.raizlabs.android.dbflow.sql.language.SQLite;
//
//import java.util.ArrayList;
//
//import butterknife.BindView;
//import in.papercrane.plyland.R;
//import in.papercrane.plyland.data.base.BaseFragment;
//import in.papercrane.plyland.data.models.Credits;
//import in.papercrane.plyland.data.models.Credits_Table;
//
///**
// * Created by PAPERCRANE on 31-01-2018.
// */
//
//public class CustomerCreditFragment extends BaseFragment {
//
//    @BindView(R.id.rv_cust_creditList)
//    RecyclerView mCreditList;
//    @BindView(R.id.toolbar_cust_credit)
//    Toolbar mToolbar;
//
//    ArrayList<Credits> creditList = new ArrayList<>();
//    CustomerCreditAdapter mAdapter;
//    Context mContext;
//
//
//    public static CustomerCreditFragment newInstance() {
//        CustomerCreditFragment fragment = new CustomerCreditFragment();
//        Bundle args = new Bundle();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//
//    @Override
//    protected void init() {
//
//        mAdapter = new CustomerCreditAdapter(creditList, mContext);
//        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext);
//        mCreditList.setLayoutManager(mLinearLayoutManager);
//        mCreditList.setItemAnimator(new DefaultItemAnimator());
//        mCreditList.setAdapter(mAdapter);
//
//    }
//
//    @Override
//    protected int setLayout() {
//        return R.layout.fragment_add_cust_credit;
//    }
//}
