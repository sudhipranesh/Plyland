package in.papercrane.plyland.data.base;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.papercrane.plyland.data.App;
import in.papercrane.plyland.data.di.components.AppComponent;

/**
 * Created by PAPERCRANE on 27-01-2018.
 */

public abstract class BaseFragment extends android.support.v4.app.Fragment {

    private Unbinder mUnbinder;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(setLayout(), container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mUnbinder = ButterKnife.bind(this, view);
        init();
    }

    public AppComponent getAppComponent() {
        return App.class.cast(getActivity()).getAppComponent();
    }

    protected abstract void init();

    @LayoutRes
    protected abstract int setLayout();
}
