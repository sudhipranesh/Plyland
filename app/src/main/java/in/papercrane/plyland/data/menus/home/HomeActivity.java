package in.papercrane.plyland.data.menus.home;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaCas;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.SessionManager;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.menus.creditList.CreditListActivity;
import in.papercrane.plyland.data.menus.customerList.CustomerListActivity;
import in.papercrane.plyland.data.menus.paymentList.PaymentListActivity;
//import in.papercrane.plyland.data.menus.reports.ReportListActivity;
import in.papercrane.plyland.data.menus.reports.ReportFilterDialogFragment;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Report;
import in.papercrane.plyland.data.models.Reports;


/**
 * Created by PAPERCRANE on 18-01-2018.
 */

public class HomeActivity extends BaseActivity {

    private static final String REPORT_FILTER = "reportFilter";

    @BindView(R.id.card_customer)
    CardView cardCustomer;
    //    @BindView(R.id.txt_filter)
//    TextView txtFilter;
    @BindView(R.id.txt_total_credits)
    TextView txtTotalCredits;

    @BindView(R.id.txt_totalCredit)
    TextView txtCredit;
//    @BindView(R.id.img_user_logout)
//    ImageView moreOption;

//    @BindView(R.id.adview_banner)
//    AdView mAdView;


    Report mReport;
    Customers mCustomers;
    double mTotalCredits = 0;
    Animation blinkAnimation;
    int i = 0;
    private InterstitialAd mInterstitialAd;
    SessionManager mSessionManager;


    public static void start(Context context) {
        Intent starter = new Intent(context, HomeActivity.class);
        context.startActivity(starter);
    }

    private ArrayList<Report> getTotalCredits() {
        ArrayList<Report> arrayList = new ArrayList<>();
        arrayList.addAll(SQLite.select()
                .from(Report.class)
                .queryList());
        return arrayList;
    }


    @Override
    protected void init() {


        creditCalculation();

//        Reports reports = new Reports();


        txtTotalCredits.setText(String.valueOf(mTotalCredits));


        i = (int) mTotalCredits;
        setUpValueAnimator();
        setUpAnimation(txtCredit);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }

    private void creditCalculation() {

        mTotalCredits = setUpTotalCredits(getTotalCredits());
    }

    private void setUpAdMob() {

        if (mInterstitialAd.isLoaded()) {

            mInterstitialAd.show();
        } else {

            Log.d("TAG", "The InterStitial Ad was Loaded");
        }

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });
    }

    private void setUpValueAnimator() {

        ValueAnimator animator = ValueAnimator.ofInt(0, i);
        animator.setDuration(6000);
        animator.addUpdateListener(animation ->
                txtTotalCredits.setText(animation.getAnimatedValue().toString()));

        animator.start();

    }

    private void setUpAnimation(TextView txtCredit) {

        blinkAnimation = AnimationUtils.loadAnimation(this, R.anim.textview_animation);

//        txtTotalCredits.setVisibility(View.VISIBLE);
        txtCredit.setVisibility(View.VISIBLE);
//        txtTotalCredits.startAnimation(blinkAnimation);
        txtCredit.startAnimation(blinkAnimation);
    }

    private double setUpTotalCredits(ArrayList<Report> totalCredits) {

        double mtotalCredit = 0;

        for (Report report : totalCredits) {

            mtotalCredit += report.totalCredits;
        }

        return mtotalCredit;
    }

    @Override
    public int setLayout() {
        return R.layout.activity_home;
    }


//    double outstandingBalance = 0;
//
//    double totalPayments;
//
//    double totalCredits;

//        List<Customers> customersList = SQLite.select()
//                .from(Customers.class)
//                .queryList();
//
//
//        for (Customers customers : customersList) {
//
//
//            customers.getPayments();
//
//
//            totalPayments = setupAddresses(customers.getPayments());
//
//            totalCredits = setupCredits(customers.getCredits());
//
//            outstandingBalance = ((customers.openingbalance) - totalPayments) + totalCredits;
//
//
//            Report report = new Report();
//            report.outstandingBalance = outstandingBalance;
//            report.customers = customers;
//            report.phone = customers.phone;
//            report.totalCredits = totalCredits;
//            report.totalPayments = totalPayments;
//            report.save();


    @OnClick(R.id.card_customer)
    public void customerActivity() {

        CustomerListActivity.start(this, null);
        setUpAdMob();
    }

    @OnClick(R.id.card_payments)
    public void openPayments() {

        PaymentListActivity.start(this, null);
    }

    @OnClick(R.id.card_credit)
    public void openCreditList() {

        CreditListActivity.start(this, null);
    }


//    @OnClick(R.id.card_reports)
//    public void openReports() {
//        ReportListActivity.start(this);
//    }

//    @OnClick(R.id.txt_filter)
//    public void openFilter() {
//
//        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
//        Fragment frag = this.getSupportFragmentManager().findFragmentByTag(REPORT_FILTER);
//        if (frag != null) {
//
//            ft.remove(frag);
//        }
//
//        ft.addToBackStack(null);
//
//        DialogFragment f = ReportFilterDialogFragment.newInstance();
//        f.show(ft, REPORT_FILTER);
//
//    }


//    @OnClick(R.id.img_user_logout)
//    public void logoutUser() {
//
//        PopupMenu popupMenu = new PopupMenu(this, moreOption);
//        popupMenu.getMenuInflater().inflate(R.menu.menu_main, popupMenu.getMenu());
//        popupMenu.setOnMenuItemClickListener(PopupMenu -> {
//
//            AlertDialog.Builder builder;
//            builder = new AlertDialog.Builder(this);
//            builder.setTitle("User Logout")
//                    .setMessage("Are You Sure You want to Logout?")
//                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
//
//                        this.userLogout();
//                        finish();
//                    })
//                    .setNegativeButton(android.R.string.no, (dialog, which) -> {
//
//                    })
//
//                    .setIcon(R.drawable.ic_user_logout)
//                    .show();
//
//            return true;
//
//        });
//
//        popupMenu.show();
//
//    }

//    private void userLogout() {
//
//        SessionManager sessionManager = new SessionManager(this);
//        sessionManager.logoutUser();
//        finish();
//    }
}










