package in.papercrane.plyland.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import in.papercrane.plyland.data.database.PlylandDB;

/**
 * Created by PAPERCRANE on 29-01-2018.
 */
@Table(database = PlylandDB.class)
public class Reports extends BaseModel implements Parcelable {

    @PrimaryKey(autoincrement = true)
    public int id;

    @Column
    public String name;

    @Column
    public String phone;

    @Column
    public int totalPayments;

    @Column
    public int totalCredits;

    @Column
    public int outStanding;

    @Column
    public double totalCredit;

    List<Customers> customers;

    List<Payments> payments;

    List<Credits> credits;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeInt(this.totalPayments);
        dest.writeInt(this.totalCredits);
        dest.writeInt(this.outStanding);
        dest.writeDouble(this.totalCredit);
    }

    public Reports() {
    }


    protected Reports(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.phone = in.readString();
        this.totalPayments = in.readInt();
        this.totalCredits = in.readInt();
        this.outStanding = in.readInt();
        this.totalCredit = in.readDouble();
    }

    public static final Parcelable.Creator<Reports> CREATOR = new Parcelable.Creator<Reports>() {
        @Override
        public Reports createFromParcel(Parcel source) {
            return new Reports(source);
        }

        @Override
        public Reports[] newArray(int size) {
            return new Reports[size];
        }
    };
}
