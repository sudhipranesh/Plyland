//package in.papercrane.plyland.data.menus.register;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import butterknife.BindView;
//import butterknife.OnClick;
//import in.papercrane.plyland.R;
//import in.papercrane.plyland.data.SessionManager;
//import in.papercrane.plyland.data.base.BaseActivity;
//import in.papercrane.plyland.data.menus.splashScreen.SplashScreenActivity;
//import in.papercrane.plyland.data.models.User;
//
///**
// * Created by PAPERCRANE on 20-02-2018.
// */
//
//public class RegisterActivity extends BaseActivity {
//
//
//    private static final String REGISTER = "registerUser";
//    private static final String USER = "user";
//    private static final String EMAIL = "email";
//    private static final String PASSWORD = "password";
//
//    @BindView(R.id.ed_reg_username)
//    EditText mUsername;
//    @BindView(R.id.ed_reg_password)
//    EditText mPassword;
//    @BindView(R.id.btn_register)
//    Button btnRegister;
//
//    User mUser;
//    String username;
//    String password;
//    SessionManager mSession;
//
//
//    public static void start(Context context) {
//        Intent starter = new Intent(context, RegisterActivity.class);
//        starter.putExtra(REGISTER, true);
//        context.startActivity(starter);
//    }
//
//    @Override
//    protected void init() {
//
//
//        mSession = new SessionManager(this);
//
//    }
//
//    @OnClick(R.id.btn_register)
//    public void register() {
//
//        userRegister();
//
//    }
//
//
//    private void userRegister() {
//
//        username = mUsername.getText().toString();
//        password = mPassword.getText().toString();
//
//        if (username.isEmpty() && password.isEmpty()) {
//
//            Toast.makeText(this, "Username or Password Field is Empty", Toast.LENGTH_SHORT).show();
//        } else {
//
//            SessionManager sessionManager = new SessionManager(getApplicationContext());
//            sessionManager.setCredentials(username, password);
//            SplashScreenActivity.start(this);
//            finish();
//        }
//    }
//
//    @Override
//    public int setLayout() {
//        return R.layout.activity_register;
//    }
//}
//
