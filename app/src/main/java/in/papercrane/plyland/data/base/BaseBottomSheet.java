package in.papercrane.plyland.data.base;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.util.DisplayMetrics;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by PAPERCRANE on 18-01-2018.
 */

public abstract class BaseBottomSheet extends BottomSheetDialogFragment {

    private Unbinder mUnbinder;

    protected abstract void init();

    @LayoutRes
    protected abstract int setLayout();

    private BottomSheetBehavior bottomSheetBehavior;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallBack = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }


        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View inflatedView = View.inflate(getContext(), setLayout(), null);
        dialog.setContentView(inflatedView);
        mUnbinder = ButterKnife.bind(this, inflatedView);


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) inflatedView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)behavior).setBottomSheetCallback(mBottomSheetBehaviorCallBack);
        }

        View parent = (View) inflatedView.getParent();
        bottomSheetBehavior = BottomSheetBehavior.from(parent);

        inflatedView.measure(0, 0);

        DisplayMetrics displayMetrics = new DisplayMetrics();   getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;

        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallBack);

        }

        params.height = screenHeight;
        parent.setLayoutParams(params);

        init();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }
}




