package in.papercrane.plyland.data.menus.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.IConditional;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.models.Credits;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;
import in.papercrane.plyland.data.models.Report;
import in.papercrane.plyland.data.models.Report_Table;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

/**
 * Created by Papercrane on 30-01-2018.
 */

public class ReportListActivity extends BaseActivity {


    @BindView(R.id.txt_credit_cust_name)
    TextView mCustomerName;
    @BindView(R.id.txt_phone_number)
    TextView mPhoneNumber;
    @BindView(R.id.txt_credits)
    TextView mCredits;
    @BindView(R.id.txt_payments)
    TextView mPayments;
    @BindView(R.id.txt_outstandingBal)
    TextView mOutstandingBal;

    public static final String CUSTOMER = "customer";

//    ReportsAdapter mAdapter;

    private Report d;

    private Customers customers;


    public static void start(Context context, Customers customers) {
        Intent starter = new Intent(context, ReportListActivity.class);
        starter.putExtra(CUSTOMER, customers);
        context.startActivity(starter);
    }

    private ArrayList<Report> getCredits(Report report) {
        ArrayList<Report> reports = new ArrayList<>();
        reports.addAll(SQLite.select()
                .from(Report.class)
                .where(Report_Table.customers_id
                        .eq((IConditional) report))
                .queryList());
        return reports;
    }

    @Override
    protected void init() {
//        String currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());
//        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
//        mPhoneNumber.setText(dateFormat.format(currentDateTimeString));
//// textView is the TextView view that should display it
//        Log.d("currentpoi", currentDateTimeString);
        customers = getIntent().getParcelableExtra(CUSTOMER);
        if (onReportsCalculated(customers).totalCredits == null) {
            Log.d("if", "if");
            onBackPressed();
//            Toast.makeText(getContext(), "Please add payment, credit for the customer", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("else", "else");
            d = onReportsCalculated(customers);
            setUpViews(d);
        }
//        mAdapter = new ReportsAdapter(this, reportArrayList(customers));
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setAdapter(mAdapter);

    }

    public void setUpViews(Report report) {

        mOutstandingBal.setText(String.valueOf(report.outstandingBalance));
        mCredits.setText(String.valueOf(report.totalCredits));
        mPayments.setText(String.valueOf(report.totalPayments));
        mCustomerName.setText(report.customers.name);
        mPhoneNumber.setText(report.customers.phone);

    }

    @Override
    public int setLayout() {
        return R.layout.activity_reposrt_item;
    }


    private Report onReportsCalculated(Customers customer) {

        double outstandingBalance = 0;

        double totalPayments;

        double totalCredits;

        double totalCreditSum;

        Report report = new Report();


        if ((customer.getCredits().isEmpty()) && (customer.getPayments().isEmpty())) {
            Log.d("anushma", "anushma");
            Toast.makeText(getContext(), "Please add payment, credit for the customer", Toast.LENGTH_LONG).show();
            return report;

        } else {

            totalPayments = setupAddresses(customer.getPayments());

            totalCredits = setupCredits(customer.getCredits());

            outstandingBalance = ((customers.openingbalance) - totalPayments) + totalCredits;

            report.outstandingBalance = outstandingBalance;
            report.customers = customers;
            report.phone = customers.phone;
            report.totalCredits = totalCredits;
            report.totalPayments = totalPayments;
            report.timestamp = java.text.DateFormat.getDateInstance().format(new Date());
            Delete.table(Report.class);
            report.save();
            return report;
        }

    }

    public Report we() {
        String date = "7 Feb 2018";
        return SQLite.select()
                .from(Report.class)
                .where(Report_Table.timestamp.between(date).and(null))
                .querySingle();

//        for (Credits allcredits : allCredits) {

    }

    private String formatDate(int year, int month, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        c.set(year, month, dayOfMonth);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Log.d("currenttime", sdf.toString());
        return sdf.format(date);
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }


    private ArrayList<Report> reportArrayList(Customers customers) {
        ArrayList<Report> reports = new ArrayList<>();
        List<Report> re = SQLite.select()
                .from(Report.class)
                .where(Report_Table.customers_id.eq(customers.id))
                .queryList();

        reports.addAll(re);
        return reports;

    }


    private Double setupAddresses(List<Payments> addresses) {
        double paymentsum = 0;


        for (Payments address : addresses) {

            paymentsum += address.amount;

        }

        return paymentsum;

    }

    private Double setupCredits(List<Credits> addresses) {
        double creditsum = 0;


        for (Credits address : addresses) {

            creditsum += address.amount;

        }

        return creditsum;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
