package in.papercrane.plyland.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import in.papercrane.plyland.data.database.PlylandDB;

/**
 * Created by Papercrane on 30-01-2018.
 */
@Table(database = PlylandDB.class)
public class Report extends BaseModel  implements Parcelable{

    @PrimaryKey(autoincrement = true)
    public int id;

    @ForeignKey(saveForeignKeyModel = true)
    public Customers customers;

    @Column
    public String phone;

    @Column
    public Double outstandingBalance;

    @Column
    public Double totalCredits;


    @Column
    public Double totalPayments;

    @Column
    public String timestamp;

    public Report() {
    }


    protected Report(Parcel in) {
        id = in.readInt();
        customers = in.readParcelable(Customers.class.getClassLoader());
        phone = in.readString();
        outstandingBalance = in.readDouble();
        totalCredits = in.readDouble();
        totalPayments = in.readDouble();
    }



    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeParcelable(customers, i);
        parcel.writeString(phone);
        parcel.writeDouble(outstandingBalance);
        parcel.writeDouble(totalCredits);
        parcel.writeDouble(totalPayments);
    }
}
