package in.papercrane.plyland.data.menus.customerList;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;


import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.menus.customerList.modification.AddCustomerFragment;
import in.papercrane.plyland.data.models.Customers;

/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class CustomerListActivity extends BaseActivity {

    private static final String CUST_LIST = "customerList";
    private static final String ADD_CUST = "addCustomer";
    private static final String CUSTOMER = "customer";

    @BindView(R.id.rv_customerlist)
    RecyclerView mCustomerList;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fab_add_customer)
    FloatingActionButton fabAddCustomer;
    @BindView(R.id.cust_progress_layout)
    ProgressRelativeLayout mProgressRelativeLayout;

    Customers mCustomer;


    private CustomerListAdapter mAdapter;

    public static void start(Context context, Customers customers) {
        Intent starter = new Intent(context, CustomerListActivity.class);
        starter.putExtra(CUST_LIST, true);
        starter.putExtra(CUSTOMER, customers);
        context.startActivity(starter);
    }


    private ArrayList<Customers> getCustomers() {
        ArrayList<Customers> customers = new ArrayList<>();
        List<Customers> customersList = SQLite.select().
                from(Customers.class).
                queryList();
        customers.addAll(customersList);
        return customers;

    }

    @Override
    protected void init() {


        mCustomer = getIntent().getParcelableExtra(CUSTOMER);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Customers");
            mToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
            mToolbar.setNavigationOnClickListener(view -> finish());
        }

        mAdapter = new CustomerListAdapter(getCustomers(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mCustomerList.setLayoutManager(linearLayoutManager);
        mCustomerList.setItemAnimator(new DefaultItemAnimator());
        mCustomerList.setAdapter(mAdapter);
    }


    @OnClick(R.id.fab_add_customer)
    public void fabClick() {

        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        Fragment frag = this.getSupportFragmentManager().findFragmentByTag(ADD_CUST);
        if (frag != null) {
            ft.remove(frag);
        }
        ft.addToBackStack(null);
        DialogFragment newFragment = AddCustomerFragment.newInstance();
        newFragment.show(ft, ADD_CUST);
    }

    @Override
    public int setLayout() {
        return R.layout.activity_customer;
    }


}
