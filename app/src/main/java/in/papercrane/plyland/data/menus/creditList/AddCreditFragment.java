package in.papercrane.plyland.data.menus.creditList;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseBottomSheet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.data.models.Credits;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;
import in.papercrane.plyland.data.models.Report;

/**
 * Created by PAPERCRANE on 18-01-2018.
 */

public class AddCreditFragment extends BaseBottomSheet {

    @BindView(R.id.spn_credit_cust)
    Spinner spCustomer;
    @BindView(R.id.ed_credit_amount)
    EditText edAmount;
    @BindView(R.id.txt_select_credit_date)
    TextView txtSelectDate;
    @BindView(R.id.ed_credit_desc)
    EditText edDescription;

    private String mSelectedDate = null;

    public static AddCreditFragment newInstance() {
        AddCreditFragment fragment = new AddCreditFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<Customers> getCustomers() {
        ArrayList<Customers> customers = new ArrayList<>();
        customers.addAll(SQLite.select().from(Customers.class).queryList());
        return customers;
    }

    @Override
    protected void init() {

        ArrayAdapter<Customers> customerAdapter = new ArrayAdapter<>
                (getContext(), android.R.layout.simple_spinner_item, getCustomers());
        customerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCustomer.setAdapter(customerAdapter);
    }

    @Override
    protected int setLayout() {
        return R.layout.fragment_add_credit;
    }

    @OnClick(R.id.iv_close_credit)
    public void close() {
        dismiss();
    }

    @OnClick(R.id.txt_select_credit_date)
    public void selectDate() {

        showDatePicker(txtSelectDate);
    }

    private void showDatePicker(TextView txtselectDate) {

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (datePicker, year, month, dayOfMonth) -> {

                    txtselectDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    mSelectedDate = formatDate(year, (month + 1), dayOfMonth);
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private String formatDate(int year, int month, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        c.set(year, month, dayOfMonth);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    @OnClick(R.id.btn_submit_credit)
    public void addCredit() {


        String amount = edAmount.getText().toString();
        String date = mSelectedDate;
        String description = edDescription.getText().toString();
        if (spCustomer == null)
            Toast.makeText(getActivity(), "Add a customer", Toast.LENGTH_SHORT).show();
        if (validateFields(amount, description, date)) {
            Credits credits = new Credits();
            credits.customers = (Customers) spCustomer.getSelectedItem();
            credits.amount = Double.parseDouble(edAmount.getText().toString());
            credits.date = mSelectedDate;
            credits.description = edDescription.getText().toString();
            credits.save();
            dismiss();
            Toast.makeText(getActivity(), "Credit Added", Toast.LENGTH_LONG).show();
            onReportsCalculated((Customers) spCustomer.getSelectedItem());
        }
    }


    public boolean validateFields(String amount, String description, String date) {
        if (amount.length() > 0 && description.length() > 0 &&
                date.length() > 0) {
            return true;
        } else {
            Toast.makeText(getActivity(), "Enter all fields", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private Report onReportsCalculated(Customers customer) {

        double outstandingBalance = 0;

        double totalPayments;

        double totalCredits;


        totalPayments = setupAddresses(customer.getPayments());

        totalCredits = setupCredits(customer.getCredits());

        outstandingBalance = ((customer.openingbalance) - totalPayments) + totalCredits;

        Log.d("outstanding balance", String.valueOf(outstandingBalance));

        Report report = new Report();
        report.outstandingBalance = outstandingBalance;
        report.customers = customer;
        report.phone = customer.phone;
        report.totalCredits = totalCredits;
        report.totalPayments = totalPayments;
        report.save();

        return report;


    }


    private Double setupAddresses(List<Payments> addresses) {
        double paymentsum = 0;


        for (Payments address : addresses) {

            paymentsum += address.amount;
        }

        return paymentsum;

    }

    private Double setupCredits(List<Credits> addresses) {
        double creditsum = 0;


        for (Credits address : addresses) {

            creditsum += address.amount;
        }

        return creditsum;

    }

}
