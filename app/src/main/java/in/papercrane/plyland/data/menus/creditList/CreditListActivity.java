package in.papercrane.plyland.data.menus.creditList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.menus.paymentList.PaymentListAdapter;
import in.papercrane.plyland.data.models.Credits;
import in.papercrane.plyland.data.models.Credits_Table;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;
import in.papercrane.plyland.data.models.Payments_Table;
import in.papercrane.plyland.data.models.Report;


/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class CreditListActivity extends BaseActivity {

    private static final String CREDIT_LIST = "creditList";
    private static final String ADD_CREDIT = "addCredit";
    private static final String CUSTOMER = "customer";

    @BindView(R.id.rv_creditlist)
    RecyclerView mCreditList;
    @BindView(R.id.credit_toolbar)
    Toolbar mToolbar;

    private Customers mCustomer;

    private ArrayList<Credits> mCredits = new ArrayList<>();
    CreditListAdapter mAdapter;

    public static void start(Context context, Customers customers) {
        Intent starter = new Intent(context, CreditListActivity.class);
        starter.putExtra(CREDIT_LIST, true);
        starter.putExtra(CUSTOMER, customers);
        context.startActivity(starter);
    }

    private ArrayList<Credits> getCredits() {
        ArrayList<Credits> credits = new ArrayList<>();
        credits.addAll(SQLite.select().from(Credits.class).queryList());
        return credits;
    }


    @Override
    protected void init() {

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Credits");
            mToolbar.setNavigationOnClickListener(view -> finish());
        }

        mCustomer = getIntent().getParcelableExtra(CUSTOMER);

        if (mCustomer != null) {
            mAdapter = new CreditListAdapter(getCreditsOfCustomer(mCustomer.id), this);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            mCreditList.setItemAnimator(new DefaultItemAnimator());
            mCreditList.setLayoutManager(mLinearLayoutManager);
            mCreditList.setAdapter(mAdapter);

        } else {


            mAdapter = new CreditListAdapter(getCredits(), this);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            mCreditList.setItemAnimator(new DefaultItemAnimator());
            mCreditList.setLayoutManager(mLinearLayoutManager);
            mCreditList.setAdapter(mAdapter);

        }
    }

    private ArrayList<Credits> getCreditsOfCustomer(int customerId) {
        ArrayList<Credits> credits = new ArrayList<>();
        List<Credits> creditsList = SQLite.select()
                .from(Credits.class)
                .where(Credits_Table.customers_id.eq(customerId))
                .queryList();
        credits.addAll(creditsList);
        return credits;
    }

    @Override
    public int setLayout() {
        return R.layout.activity_credit;
    }


    @OnClick(R.id.fab_add_credit)
    public void addCredits() {

        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        Fragment frag = this.getSupportFragmentManager().findFragmentByTag(ADD_CREDIT);
        if (frag != null) {
            ft.remove(frag);
        }
        ft.addToBackStack(null);

        AddCreditFragment newFragment = AddCreditFragment.newInstance();
        newFragment.show(ft, ADD_CREDIT);

        onReportsCalculated();


    }

    private double onReportsCalculated() {

        double outstandingBalance = 0;

        double totalPayments;

        double totalCredits;

        List<Customers> customersList = SQLite.select()
                .from(Customers.class)
                .queryList();

        for (Customers customers : customersList) {

            Log.d("customer", customers.name);
            customers.getPayments();
            Log.d("paymentslist", customers.getPayments().toString());

            totalPayments = setupAddresses(customers.getPayments());

            totalCredits = setupCredits(customers.getCredits());

            outstandingBalance = ((customers.openingbalance) - totalPayments) + totalCredits;

            Log.d("outstanding balance", String.valueOf(outstandingBalance));

            Report report = new Report();
            report.outstandingBalance = outstandingBalance;
            report.customers = customers;
            report.phone = customers.phone;
            report.totalCredits = totalCredits;
            report.totalPayments = totalPayments;
            report.save();
        }

        return outstandingBalance;
    }


    private Double setupAddresses(List<Payments> addresses) {
        double paymentsum = 0;


        for (Payments address : addresses) {

            paymentsum += address.amount;
            Log.d("paymentsum", String.valueOf(paymentsum));
        }

        return paymentsum;

    }

    private Double setupCredits(List<Credits> addresses) {
        double creditsum = 0;


        for (Credits address : addresses) {

            creditsum += address.amount;
            Log.d("paymentsum", String.valueOf(creditsum));
        }

        return creditsum;

    }

}
