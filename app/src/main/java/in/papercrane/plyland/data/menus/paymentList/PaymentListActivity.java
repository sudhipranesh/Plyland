package in.papercrane.plyland.data.menus.paymentList;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;


import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.base.BaseActivity;
import in.papercrane.plyland.data.models.Customers;
import in.papercrane.plyland.data.models.Payments;
import in.papercrane.plyland.data.models.Payments_Table;


/**
 * Created by PAPERCRANE on 16-01-2018.
 */

public class PaymentListActivity extends BaseActivity {

    public static final String PAYMENT_LIST = "paymentList";
    public static final String ADD_PAYMENT = "addPayment";
    public static final String CUSTOMER = "customer";

    @BindView(R.id.rv_payment_list)
    RecyclerView mPaymentList;
    @BindView(R.id.toolbar_payment)
    Toolbar mToolbar;

    private Customers mCustomer;


    private PaymentListAdapter mAdapter;

    public static void start(Context context, Customers customer) {
        Intent starter = new Intent(context, PaymentListActivity.class);
        starter.putExtra(PAYMENT_LIST, true);
        starter.putExtra(CUSTOMER, customer);
        context.startActivity(starter);
    }

    private ArrayList<Payments> getPayments() {
        ArrayList<Payments> payments = new ArrayList<>();
        List<Payments> mPaymentList = SQLite.select()
                .from(Payments.class)
                .queryList();
        payments.addAll(mPaymentList);
        return payments;
    }

    private ArrayList<Payments> getPaymentsOfCustomer(int customerId) {
        ArrayList<Payments> payments = new ArrayList<>();
        List<Payments> paymentsList = SQLite.select()
                .from(Payments.class)
                .where(Payments_Table.customers_id.eq(customerId))
                .queryList();
        payments.addAll(paymentsList);
        return payments;
    }


    @Override
    protected void init() {

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Payments");
            mToolbar.setNavigationOnClickListener(view -> finish());
        }

        mCustomer = getIntent().getParcelableExtra(CUSTOMER);


        if (mCustomer != null) {

            mCustomer = getIntent().getParcelableExtra(CUSTOMER);
            mAdapter = new PaymentListAdapter(getPaymentsOfCustomer(mCustomer.id), this);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            mPaymentList.setLayoutManager(mLinearLayoutManager);
            mPaymentList.setItemAnimator(new DefaultItemAnimator());
            mPaymentList.setAdapter(mAdapter);
        } else {

            mAdapter = new PaymentListAdapter(getPayments(), this);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            mPaymentList.setLayoutManager(mLinearLayoutManager);
            mPaymentList.setItemAnimator(new DefaultItemAnimator());
            mPaymentList.setAdapter(mAdapter);
        }
    }

    @OnClick(R.id.fab_add_payments)
    public void addPayments() {

        FragmentTransaction ft = this.getSupportFragmentManager().beginTransaction();
        Fragment frag = this.getSupportFragmentManager().findFragmentByTag(ADD_PAYMENT);
        if (frag != null) {
            ft.remove(frag);
        }
        ft.addToBackStack(null);

        android.support.v4.app.DialogFragment newFragment = AddPaymentFragment.newInstatnce();
        newFragment.show(ft, ADD_PAYMENT);
    }

    @Override
    public int setLayout() {
        return R.layout.activity_payments;
    }
}
