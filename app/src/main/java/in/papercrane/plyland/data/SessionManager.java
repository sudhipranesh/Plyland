package in.papercrane.plyland.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;

//import in.papercrane.plyland.data.menus.login.LoginActivity;
import in.papercrane.plyland.data.models.User;


/**
 * Created by PAPERCRANE on 20-02-2018.
 */

public class SessionManager {

    private static final String TAG = "tag";

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "plyland";
    private static final String IS_LOGIN = "isLoggedIn";
    private static final String PASSWORD = "name";
    private static final String EMAIL = "email";

    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;
    Context mContext;
    User user;

    public SessionManager(Context context) {

        this.mContext = context;
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mSharedPreferences.edit();
        mEditor.apply();

    }

    public void setCredentials(String email, String password) {
        mEditor.putString(EMAIL, email);
        mEditor.putString(PASSWORD, password);
        mEditor.commit();
    }

    public String getEmail() {
        return mSharedPreferences.getString(EMAIL, null);
    }

    public String getPassword() {

        return mSharedPreferences.getString(PASSWORD, null);
    }


    public void createLoginSession(String email, String password) {


        Log.e(TAG, "createLogin()");
        mEditor.putBoolean(IS_LOGIN, true);
        mEditor.putString(EMAIL, email);
        mEditor.putString(PASSWORD, password);
        mEditor.commit();
    }

    public void getLoginUser(String email, String password) {

//        mSharedPreferences.getBoolean(IS_LOGIN, true);
        mSharedPreferences.getString(EMAIL, email);
        mSharedPreferences.getString(PASSWORD, password);
        mEditor.commit();
    }

    public HashMap<String, String> getUserDetails() {

        HashMap<String, String> user = new HashMap<>();

        user.put(PASSWORD, mSharedPreferences.getString(PASSWORD, null));
        user.put(EMAIL, mSharedPreferences.getString(EMAIL, null));

        return user;
    }
//
//    public void checkLogin() {
//
//        if (!this.isloggedIn()) {
//
//            LoginActivity.start(mContext);
//        }
//    }

//    public void logoutUser() {
//
//        LoginActivity.start(mContext);
//    }

    public boolean isloggedIn() {

        return mSharedPreferences.getBoolean(IS_LOGIN, false);
    }
}
