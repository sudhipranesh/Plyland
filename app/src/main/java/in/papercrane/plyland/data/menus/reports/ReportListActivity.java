//package in.papercrane.plyland.data.menus.reports;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.annotation.FontRes;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//
//import com.raizlabs.android.dbflow.sql.language.SQLite;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.BindView;
//import in.papercrane.plyland.R;
//import in.papercrane.plyland.data.base.BaseActivity;
//import in.papercrane.plyland.data.models.Credits;
//import in.papercrane.plyland.data.models.Customers;
//import in.papercrane.plyland.data.models.Payments;
//import in.papercrane.plyland.data.models.Report;
//
///**
// * Created by Papercrane on 30-01-2018.
// */
//
//public class ReportListActivity extends BaseActivity {
//
//    private static final String REPORT_LIST = "reportList";
//
//    @BindView(R.id.rv_reports_list)
//    RecyclerView mReportRecyclerView;
//    @BindView(R.id.reports_toolbar)
//    Toolbar mToolbar;
//
//    private ReportsAdapter mAdapter;
//
//    private double payments = 0;
//
//
//    public static void start(Context context) {
//        Intent starter = new Intent(context, ReportListActivity.class);
//        starter.putExtra(REPORT_LIST, true);
//        context.startActivity(starter);
//    }
//
//
//    private ArrayList<Report> reportArrayList() {
//        ArrayList<Report> reports = new ArrayList<>();
//        List<Report> reportsList = SQLite.select()
//                .from(Report.class)
//                .queryList();
//
//        reports.addAll(reportsList);
//
//        return reports;
//    }
//
//
//    @Override
//    protected void init() {
//
//        setSupportActionBar(mToolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("Reports");
//            mToolbar.setNavigationOnClickListener(view -> finish());
//        }
//
//        mAdapter = new ReportsAdapter(this, reportArrayList());
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        mReportRecyclerView.setLayoutManager(linearLayoutManager);
//        mReportRecyclerView.setAdapter(mAdapter);
//
//        onReportsCalculated();
//    }
//
//    @Override
//    public int setLayout() {
//        return R.layout.activity_reports_list;
//    }
//
//    private double onReportsCalculated() {
//
//        double outstandingBalance = 0;
//
//        double totalPayments;
//
//        double totalCredits;
//
//        List<Customers> customersList = SQLite.select()
//                .from(Customers.class)
//                .queryList();
//
//
//        for (Customers customers : customersList) {
//
//            Log.d("customer", customers.name);
//            customers.getPayments();
//            Log.d("paymentslist", customers.getPayments().toString());
//
//            totalPayments = setupAddresses(customers.getPayments());
//
//            totalCredits = setupCredits(customers.getCredits());
//
//            outstandingBalance = ((customers.openingbalance) - totalPayments) + totalCredits;
//            Log.d("outstanding balance", String.valueOf(outstandingBalance));
//
//            Report report = new Report();
//            report.outstandingBalance = outstandingBalance;
//            report.customers = customers;
//            report.phone = customers.phone;
//            report.totalCredits = totalCredits;
//            report.totalPayments = totalPayments;
//            report.save();
//
//        }
//
//        return outstandingBalance;
//    }
//
//    private Double setupAddresses(List<Payments> addresses) {
//        double paymentsum = 0;
//
//        for (Payments address : addresses) {
//
//            paymentsum += address.amount;
//            Log.d("paymentsum", String.valueOf(paymentsum));
//        }
//
//        return paymentsum;
//
//    }
//
//    private Double setupCredits(List<Credits> addresses) {
//        double creditsum = 0;
//
//
//        for (Credits address : addresses) {
//
//            creditsum += address.amount;
//            Log.d("paymentsum", String.valueOf(creditsum));
//        }
//
//        return creditsum;
//    }
//
//}
