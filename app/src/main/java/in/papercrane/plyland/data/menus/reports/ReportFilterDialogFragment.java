package in.papercrane.plyland.data.menus.reports;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.papercrane.plyland.R;
import in.papercrane.plyland.data.models.Report;
import in.papercrane.plyland.data.models.Report_Table;


/**
 * Created by PAPERCRANE on 05-02-2018.
 */


public class ReportFilterDialogFragment extends DialogFragment {

    @BindView(R.id.txt_start_date)
    TextView txtStartDate;
    @BindView(R.id.txt_end_date)
    TextView txtEndDate;
    @BindView(R.id.toolbar_filter)
    Toolbar mToolbar;

    String mSelectedStartDate = null;
    String mSelectedEndDate = null;
    Report report;

    public static ReportFilterDialogFragment newInstance() {
        ReportFilterDialogFragment fragment = new ReportFilterDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report_filter_dialog, container, false);
        ButterKnife.bind(this, view);

        return view;

    }

    @OnClick(R.id.txt_start_date)
    public void openDatePicker() {

        showDatePicker(txtStartDate);
    }

//    private ArrayList<Report> getReports() {
//        ArrayList<Report> mReport = new ArrayList<>();
//        SQLite.select().from(Report.class)
//                .where(Report_Table.timestamp
//                        .between(mSelectedStartDate)
//                        .and(mSelectedEndDate))
//                .queryList();
//        return mReport;
//    }

    private void showDatePicker(final TextView txtDate) {

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (view, year, month, dayOfMonth) -> {

                    txtDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    mSelectedStartDate = formatDate(year, (month + 1), dayOfMonth);

                }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    private String formatDate(int year, int month, int dayOfMonth) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(0);
        c.set(year, month, dayOfMonth);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }


    @OnClick(R.id.txt_end_date)
    public void showDate() {

        openDatePicker(txtEndDate);
    }

    private void openDatePicker(TextView txtEndDate) {

        Calendar cal = Calendar.getInstance();
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (view, year, month, dayOfMonth) -> {

                    txtEndDate.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
                    mSelectedEndDate = formatDate(year, (month + 1), dayOfMonth);

                }, mYear, mMonth, mDayOfMonth);

        datePickerDialog.show();
    }

    @OnClick(R.id.btn_apply_filter)
    public void filterApply() {

        String endDate = txtEndDate.getText().toString();
        String startDate = txtStartDate.getText().toString();
        Report report = SQLite.select()
                .from(Report.class)
                .where(Report_Table.timestamp.eq(startDate))
                .querySingle();

        Log.d("report", String.valueOf(report));

        ReportFilterActivity.start(getContext(), report);
    }

}
