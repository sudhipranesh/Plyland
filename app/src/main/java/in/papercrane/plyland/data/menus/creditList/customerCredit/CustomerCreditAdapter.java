//package in.papercrane.plyland.data.menus.creditList.customerCredit;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//import java.util.ArrayList;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import in.papercrane.plyland.R;
//import in.papercrane.plyland.data.models.Credits;
//
///**
// * Created by PAPERCRANE on 31-01-2018.
// */
//
//public class CustomerCreditAdapter extends RecyclerView.Adapter<CustomerCreditAdapter.CustomerViewHolder> {
//
//    ArrayList<Credits> mCreditList;
//    Context mContext;
//
//    public CustomerCreditAdapter(ArrayList<Credits> creditList, Context mContext) {
//        this.mCreditList = creditList;
//        this.mContext = mContext;
//    }
//
//    @Override
//    public CustomerCreditAdapter.CustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        View view = LayoutInflater.from(mContext)
//                .inflate(R.layout.activity_credit_list_item, parent, false);
//
//        return new CustomerViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(CustomerCreditAdapter.CustomerViewHolder holder, int position) {
//        Credits credits = mCreditList.get(position);
//        holder.txtDate.setText(credits.date);
//        holder.txtAmount.setText(String.valueOf(credits.amount));
//        holder.txtDescription.setText(credits.description);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mCreditList.size();
//    }
//
//    public class CustomerViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.txt_cust_credit)
//        TextView txtDate;
//        @BindView(R.id.txt_cust_credit_amnt)
//        TextView txtAmount;
//        @BindView(R.id.txt_cust_credit_desc)
//        TextView txtDescription;
//
//        public CustomerViewHolder(View itemView) {
//            super(itemView);
//
//            ButterKnife.bind(itemView);
//        }
//    }
//}
