package in.papercrane.plyland.data.base;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.papercrane.plyland.data.App;
import in.papercrane.plyland.data.di.components.AppComponent;

/**
 * Created by PAPERCRANE on 18-01-2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView {

    private Unbinder mUnbinder;

    protected  abstract void init();

    @LayoutRes
    public abstract int setLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(setLayout());
        mUnbinder = ButterKnife.bind(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mUnbinder.unbind();
    }

   public AppComponent getAppComponent() {
        return App.class.cast(getApplication()).getAppComponent();
  }
}
